package se.rende.krequests

import com.fasterxml.jackson.databind.ObjectMapper
import java.io.DataOutputStream
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.nio.charset.Charset

private fun <K, V> Map<K, V>.ifNotEmpty(function: (map: Map<K, V>) -> Unit) {
    if (this.isNotEmpty()) {
        function(this)
    }
}

typealias Headers = Map<String, String>
typealias Cookies = Map<String, String>

private class Method {
    companion object {
        val GET = "GET"
        val POST = "POST"
        val DELETE = "DELETE"
        val HEAD = "HEAD"
        val PUT = "PUT"
        val PATCH = "PATCH"
        val OPTIONS = "OPTIONS"
        val CONNECT = "CONNECT"
        val TRACE = "TRACE"
    }
}

private class ContentType {
    companion object {
        val formUrlEncoded = "application/x-www-form-urlencoded"
        val formData = "application/x-www-form-urlencoded"
        val textPlain = "text/plain"
    }
}

internal fun doGet(url: String, headers: Headers, cookies: Cookies): KRequests.HttpResponse {
    val connection = URL(url).openConnection() as HttpURLConnection

    connection.requestMethod = Method.GET

    addHeaders(headers, connection)

    addCookies(cookies, connection)

    connection.connect()

    return extractResponseData(connection)
}

internal fun doHead(url: String, headers: Headers, cookies: Cookies): KRequests.HttpResponse {
    val connection = URL(url).openConnection() as HttpURLConnection

    connection.requestMethod = Method.HEAD

    addHeaders(headers, connection)

    addCookies(cookies, connection)

    connection.connect()

    return extractResponseData(connection)
}

internal fun doDelete(url: String, headers: Headers, cookies: Cookies): KRequests.HttpResponse {
    val connection = URL(url).openConnection() as HttpURLConnection

    connection.requestMethod = Method.DELETE

    addHeaders(headers, connection)

    addCookies(cookies, connection)

    connection.connect()

    return extractResponseData(connection)
}

internal fun doOptions(url: String, headers: Headers, cookies: Cookies): KRequests.HttpResponse {
    throw RuntimeException("Not implemented")
}

internal fun doConnect(url: String, headers: Headers, cookies: Cookies): KRequests.HttpResponse {
    throw RuntimeException("Not implemented")
}

internal fun doTrace(url: String, headers: Headers, cookies: Cookies): KRequests.HttpResponse {
    throw RuntimeException("Not implemented")
}

internal fun doPost(url: String, body: String, headers: Headers, cookies: Cookies): KRequests.HttpResponse {
    val connection = URL(url).openConnection() as HttpURLConnection

    connection.requestMethod = Method.POST

    addHeaders(headers, connection)

    addCookies(cookies, connection)

    if (body.isNotBlank()) {
        if (connection.getRequestProperty("Content-Type").isNullOrEmpty()) {
            System.err.println("Warning: Adding request body without Content-Type is not recommended")
        }
        connection.doOutput = true
        DataOutputStream(connection.outputStream).use {
            it.writeBytes(body)
            it.flush()
        }
    }

    connection.connect()

    return extractResponseData(connection)
}

internal fun doPostWithForm(url: String, form: Map<String, String>, charset: Charset, headers: Headers, cookies: Cookies): KRequests.HttpResponse {
    val connection = URL(url).openConnection() as HttpURLConnection

    connection.requestMethod = Method.POST

    addHeaders(headers, connection)

    addCookies(cookies, connection)

    if (connection.getRequestProperty("charset") == null) {
        connection.setRequestProperty("charset", charset.name().toLowerCase())
    }

    if (form.isNotEmpty()) {
        var contentType = connection.getRequestProperty("Content-Type")
        if (contentType.isNullOrEmpty()) {
            System.out.println("INFO: Setting Content-Type to ${ContentType.formUrlEncoded}")
            connection.setRequestProperty("Content-Type", ContentType.formUrlEncoded)
            contentType = ContentType.formUrlEncoded
        }
        connection.doOutput = true
        connection.useCaches = false
        val output = form
                .map { it.key + '=' + it.value }
                .reduce { acc, s -> acc.plus("&").plus(s) }
        connection.setRequestProperty("Content-Length", Integer.toString(output.length))

        DataOutputStream(connection.outputStream).use {
            if (contentType == ContentType.formUrlEncoded || contentType == ContentType.textPlain) {
                if (charset == Charsets.UTF_8) {
                    it.writeUTF(output)
                } else {
                    it.writeChars(output)
                }
            } else if (contentType == ContentType.formData) {
                it.writeBytes(output)
            }
            it.flush()
        }
    }

    connection.connect()

    return extractResponseData(connection)
}

internal fun extractResponseData(connection: HttpURLConnection): KRequests.HttpResponse {
    val status = connection.headerFields[null]!![0].split(" ")[1].toInt()

    var text = ""
    InputStreamReader(connection.content as InputStream?).use {
        text = it.readText()
    }

    val encoding = if (connection.contentEncoding != null) connection.contentEncoding else "utf-8"

    return KRequests.HttpResponse(status, text, connection.headerFields, encoding)
}

private fun addCookies(cookies: Cookies, connection: HttpURLConnection) {
    cookies.ifNotEmpty {
        connection.addRequestProperty("Cookie", it
                .map { it.key + "=" + it.value }
                .reduce { acc, s -> acc.plus(s + ";") })
    }
}

private fun addHeaders(headers: Headers, connection: HttpURLConnection) {
    headers.forEach {
        connection.addRequestProperty(it.key, it.value)
    }
}

class KRequests {
    class HttpResponse(val statusCode: Int, val text: String, val headerFields: Map<String, List<String>>, val encoding: String) {
        fun json(): HashMap<String, Any> {
            if (text.isBlank()) {
                return HashMap(0)
            }
            return ObjectMapper().readValue(text, HashMap<String, Any>().javaClass)
        }
    }

    companion object {
        fun get(url: String, headers: Headers = mapOf(), cookies: Cookies = mapOf()): HttpResponse = doGet(url, headers, cookies)

        fun post(url: String, body: String = "", headers: Headers = mapOf(), cookies: Cookies = mapOf()): HttpResponse = doPost(url, body, headers, cookies)

        fun post(url: String, form: Map<String, String> = mapOf(), headers: Headers = mapOf(), cookies: Cookies = mapOf()): HttpResponse = doPostWithForm(url, form, Charset.forName("UTF-8"), headers, cookies)

        fun post(url: String, formAndCharset: Pair<Map<String, String>, Charset>, headers: Headers = mapOf(), cookies: Cookies = mapOf()): HttpResponse = doPostWithForm(url, formAndCharset.first, formAndCharset.second, headers, cookies)

        fun delete(url: String, headers: Headers = mapOf(), cookies: Cookies = mapOf()): HttpResponse = doDelete(url, headers, cookies)

        fun head(url: String, headers: Headers = mapOf(), cookies: Cookies = mapOf()): HttpResponse = doHead(url, headers, cookies)

        fun options(url: String, headers: Headers = mapOf(), cookies: Cookies = mapOf()): HttpResponse = doOptions(url, headers, cookies)

        fun connect(url: String, headers: Headers = mapOf(), cookies: Cookies = mapOf()): HttpResponse = doConnect(url, headers, cookies)

        fun trace(url: String, headers: Headers = mapOf(), cookies: Cookies = mapOf()): HttpResponse = doTrace(url, headers, cookies)
    }
}