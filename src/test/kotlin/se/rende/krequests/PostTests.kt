package se.rende.krequests

import kotlin.test.assertEquals
import org.junit.Test
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.Ignore
import java.util.LinkedHashMap
import kotlin.test.assertFalse
import kotlin.test.assertTrue
import kotlin.test.fail

class PostTests {

    @Test fun shouldSupportEmptyBody() {
        println("-----------POST + EMPTY BODY-----------")
        KRequests.post("http://httpbin.org/post", "").apply {
            println(statusCode)
            println(encoding)
            println(headerFields)
            println(text)
            println(json())

            assertEquals(statusCode, 200)
            assertTrue(text.contains("\"form\": {},"))
            assertTrue(text.contains("\"data\": \"\","))
            assertTrue(text.contains("\"files\": {},"))
            assertTrue(text.contains("\"args\": {},"))
        }
    }

    @Test fun shouldSupportJsonBody() {
        println("-----------POST + JSON BODY-----------")
        val jsonBody = ObjectMapper().writeValueAsString(mapOf("key" to "value"))
        KRequests.post("http://httpbin.org/post", jsonBody,
                mapOf("Content-Type" to "application/json; charset=UTF-8")).apply {

            println(statusCode)
            println(encoding)
            println(headerFields)
            println(text)
            println(json())

            assertEquals(statusCode, 200)
            assertTrue(text.contains("\"form\": {},"))
            assertTrue(text.contains("\"data\": \"{\\\"key\\\":\\\"value\\\"}\","))
            assertEquals((json()["json"] as LinkedHashMap<*, *>)["key"], "value")
        }
    }

    @Test fun shouldSupportFormBody() {
        println("-----------POST + FORM BODY-----------")
        KRequests.post("http://httpbin.org/post",
                form = mapOf("myKey" to "myValue", "myOtherKey" to "myOtherValue")).apply {
            println(statusCode)
            println(encoding)
            println(headerFields)
            println(text)
            println(json())

            assertEquals(statusCode, 200)
            assertFalse(text.contains("""
"form": {
    "\u0000%myKey": "myValue",
    "myOtherKey": "myOtherValue"
}"""))
            assertTrue(text.contains("\"data\": \"\","))
            assertTrue(text.contains("\"files\": {},"))
            assertTrue(text.contains("\"args\": {},"))
        }
    }

    @Ignore
    @Test fun shouldSupportFormBodyWithAsciiEncoding() {
        fail("WIP")
    }

    @Ignore
    @Test fun shouldSupportFormBodyWithBinaryData() {
        fail("WIP")
    }
}
