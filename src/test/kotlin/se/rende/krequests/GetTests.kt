package se.rende.krequests

import kotlin.test.assertEquals
import org.junit.Test

class GetTests {

    @Test fun getRequestShouldReturn200AndBody() {
        println("-----------GET-----------")
        KRequests.get("http://httpbin.org/get").apply {
            println(statusCode)
            println(encoding)
            println(headerFields)
            println(text)
            println(json())

            assertEquals(statusCode, 200)
        }
    }

    @Test fun getRequestShouldAllowHeaders() {
        println("-----------GET + HEADERS-----------")
        KRequests.get("http://httpbin.org/headers", mapOf("myHeader" to "1337")).apply {
            println(statusCode)
            println(encoding)
            println(headerFields)
            println(text)
            println(json())

            assertEquals(statusCode, 200)
        }
    }

    @Test fun getRequestShouldAllowCookies() {
        println("-----------GET + COOKIES-----------")
        KRequests.get("http://httpbin.org/cookies", cookies = mapOf("myCookie" to "1337")).apply {
            println(statusCode)
            println(encoding)
            println(headerFields)
            println(text)
            println(json())

            assertEquals(statusCode, 200)
        }
    }

    @Test fun getRequestShouldAllowSettingCustomUserAgent() {
        println("-----------GET + USER-AGENT-----------")
        KRequests.get("http://httpbin.org/user-agent", mapOf("User-Agent" to "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0")).apply {
            println(statusCode)
            println(encoding)
            println(headerFields)
            println(text)
            println(json())

            assertEquals(statusCode, 200)
        }
    }
}