package se.rende.krequests

import org.junit.Test
import kotlin.test.assertEquals

class DeleteTests {

    @Test fun deleteRequestShouldReturn200AndBody() {
        println("-----------DELETE-----------")
        KRequests.delete("http://httpbin.org/delete").apply {
            println(statusCode)
            println(encoding)
            println(headerFields)
            println(text)
            println(json())

            assertEquals(statusCode, 200)
            assertEquals(json()["url"], "http://httpbin.org/delete")
        }
    }
}
