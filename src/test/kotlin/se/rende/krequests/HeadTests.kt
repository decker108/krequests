package se.rende.krequests

import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class HeadTests {

    @Test fun headRequestShouldReturn200ButNoBody() {
        println("-----------HEAD-----------")
        val r = KRequests.head("http://httpbin.org/get").apply {
            println(statusCode)
            println(encoding)
            println(headerFields)
            println(text)
            println(json())
    
            assertEquals(statusCode, 200)
            assertTrue(text.isEmpty())
            assertTrue(json().isEmpty())
        }
    }
}
